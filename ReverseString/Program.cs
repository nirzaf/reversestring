﻿using System;

namespace ReverseString
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine(ReversedString("nirzaF"));
            Console.WriteLine(IsPalindrome("Madam"));
            Console.ReadLine();
        }

        static string ReversedString(string str) {
            try {
                char[] charArray = str.ToCharArray();
                for (int i = 0, j = str.Length - 1; i < j; i++, j--) {
                    charArray[i] = str[j];
                    charArray[j] = str[i];
                }
                string reversedstring = new string(charArray);
                return reversedstring;
            } catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
                throw;
            }
        }

        public static string IsPalindrome(string str)
        {
            str = str.ToLower();
            for (int i = 0, j = str.Length - 1; i < str.Length / 2; i++, j--)
            {
                if (str[i] != str[j])
                {
                    return "Not Palindrome";
                }                    
            }
            return "Palindrome";
        }
    }
}
